/*
** my_printf.c for my_printf.c in /home/el-mou_r/rendu/PSU_2015_my_printf
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Nov  7 23:13:00 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 15 18:07:01 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdarg.h>
#include "../include/my.h"

void		next_my_printf(t_struct *st, char *s, t_function *array_pointers)
{
  st->ct = 0;
  while (st->flag_model[st->ct] != s[st->i] && st->flag_model[st->ct] != '\0')
    st->ct++;
  if (st->flag_model[st->ct] == '\0')
    my_putchar(s[st->i], st);
  if (st->ct <= 11)
    {
      if (st->restart != 1 && st->ct < 11)
	{
	  check_tour(st, s);
	  st->nb_tour++;
	}
      (array_pointers)[st->ct].p(st->va, st);
    }
}

t_function	*filling_array(t_function *ar)
{
  if ((ar = malloc(sizeof(t_function) * 12)) == NULL)
    return (NULL);
  ar[0].p = &print_i;
  ar[1].p = &print_d;
  ar[2].p = &print_u;
  ar[3].p = &print_c;
  ar[4].p = &print_s;
  ar[5].p = &print_S;
  ar[6].p = &print_b;
  ar[8].p = &print_x;
  ar[9].p = &print_X;
  ar[7].p = &print_p;
  ar[10].p = &print_o;
  ar[11].p = &print_prc;
  return (ar);
}

void		init_my_variables(t_struct *st)
{
  st->i = 0;
  st->ct = 0;
  st->nb_char = 0;
  st->nb_tour = 0;
  st->save_po = 0;
  st->pass = 0;
  st->restart = 0;
  st->quit = 0;
  st->flag_model = "iducsSbpxXo%";
}

int		my_printf(char *s, ...)
{
  t_struct	st;
  t_function	*array_pointers;

  init_my_variables(&st);
  if ((array_pointers = filling_array(array_pointers)) == NULL)
    return (0);
  while (s[st.i] != '\0')
    {
      st.ct = 0;
      if (s[st.i] == '%' && s[st.i + 1] == '\0')
	st.quit = 1;
      if (s[st.i] != '%' && st.quit == 0)
	my_putchar(s[st.i], &st);
      else if (s[st.i] == '%' && st.quit == 0)
	{
	  st.i++;
	  va_start(st.va, s);
	  check_$(&st, s);
	  next_my_printf(&st, s, array_pointers);
	}
      st.i++;
      st.restart = 0;
      st.pass = 0;
    }
  va_end(st.va);
  free(array_pointers);
  return (st.nb_char);
}
