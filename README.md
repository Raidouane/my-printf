my_printf

int my_printf ( const char * format, ... );

Print formatted data to stdout

Writes the C string pointed by format to the standard output (stdout). If format includes format specifiers (subsequences beginning with %), the additional arguments following format are formatted and inserted in the resulting string replacing their respective specifiers.

http://www.cplusplus.com/reference/cstdio/printf/

Made in 2015
