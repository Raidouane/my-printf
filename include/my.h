/*
** my.h for my.h in /home/el-mou_r/rendu/PSU_2015_my_printf
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Nov  7 23:16:22 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 15 18:06:52 2015 Raidouane EL MOUKHTARI
*/

#ifndef _MY_H
# define _MY_H
# include <stdarg.h>

typedef struct	s_struct
{
  int		i;
  int		nb_tour;
  int		nb_char;
  int		pass;
  int		restart;
  int		quit;
  int		ct;
  char		*flag_model;
  char		*sa;
  char		c;
  int		y;
  int		save_po;
  int		ar;
  va_list	va;
}		t_struct;

typedef	struct	s_function
{
  void	(*p)(va_list va, t_struct *st);
}		t_function;

int		get_nbr1(char *str, int a, int na);
int		my_getnbr(char *str);
void		my_putchar(char c, t_struct *);
void		my_put_nbr(int nb, t_struct *);
char		*my_strcpy(char *src, char *dest);
int		my_strlen(char *str);
char		*my_revstr(char *str);
void		 my_putstr_S_suite(char *recup, t_struct *);;
void		my_putstr_S(char *str, t_struct *);
void		my_putstr(char *str, t_struct *);
char		*my_getbase(unsigned int, char *, t_struct *);
char		*my_getbase_S(unsigned int, char *);
char		*my_getbase_pointer(long int, char *, t_struct *);
t_function	*filling_array(t_function *);
int		my_printf(char *s, ...);
void		next_my_printf(t_struct *st, char *s, t_function *array_pointers);
void		 next_manage_$(t_struct *st, char *s);
void		init_my_variables(t_struct *st);
void		check_$(t_struct *st, char *s);
void		check_spaces(t_struct *st, char *s);
void		check_tour(t_struct *st, char *s);
void		print_i(va_list va, t_struct *);
void		print_u(va_list va, t_struct *);
void		print_d(va_list va, t_struct *);
void		print_c(va_list va, t_struct *);
void		print_s(va_list va, t_struct *);
void		print_S(va_list va, t_struct *);
void		print_b(va_list va, t_struct *);
void		print_x(va_list va, t_struct *);
void		print_X(va_list va, t_struct *);
void		print_p(va_list va, t_struct *);
void		print_o(va_list va, t_struct *);
void		print_prc(va_list va, t_struct *);

#endif
