##
## Makefile for Makefile in /home/el-mou_r/rendu/PSU_2015_my_printf_bootstrap
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Wed Nov  4 14:55:50 2015 Raidouane EL MOUKHTARI
## Last update Fri Nov 13 21:47:25 2015 Raidouane EL MOUKHTARI
##

#include "include/my.h"

SRC	= src/my_putchar.c		\
	src/my_printf.c			\
	src/my_putstr.c			\
	src/my_put_nbr.c		\
	src/my_putbase.c		\
	src/my_putbase_s.c		\
	src/my_putbase_pointer.c	\
	src/flago.c			\
	src/flagsbpxx.c			\
	src/my_getnbr.c			\
	src/my_put_nbr_u.c		\
	src/my_putstr_s.c		\
	src/my_revstr.c			\
	src/flag_iudcs.c		\
	src/my_strlen.c			\
	src/my_check_of_printf.c

NAME	= libmy.a

OBJ	= $(SRC:.c=.o)

$(NAME):	$(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

all:	$(NAME)

clean:
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all
